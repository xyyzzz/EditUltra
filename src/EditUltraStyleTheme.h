#ifndef _H_EDITULTRA_STYLETHEME_
#define _H_EDITULTRA_STYLETHEME_

#include "framework.h"

struct StyleClass
{
	char			font[ 32 ] ;
	int			fontsize ;
	int			color ;
	BOOL			bold ;
};

struct StyleTheme
{
	struct StyleClass	text ;
	struct StyleClass	caretline ;

	struct StyleClass	keywords ;
	struct StyleClass	keywords2 ;
	struct StyleClass	string ;
	struct StyleClass	character ;
	struct StyleClass	number ;
	struct StyleClass	operatorr ;
	struct StyleClass	preprocessor ;
	struct StyleClass	comment ;
	struct StyleClass	commentline ;
	struct StyleClass	commentdoc ;

	struct StyleClass	tags ;
	struct StyleClass	unknowtags ;
	struct StyleClass	attributes ;
	struct StyleClass	unknowattributes ;
	struct StyleClass	entities ;
	struct StyleClass	tagends ;
	struct StyleClass	cdata ;
	struct StyleClass	phpsection ;
	struct StyleClass	aspsection ;
} ;

void SetStyleThemeDefault( struct StyleTheme *pstStyleTheme );
int LoadStyleThemeConfigFile( char *filebuf );
int SaveStyleThemeConfigFile();

INT_PTR CALLBACK StyleThemeWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

#endif

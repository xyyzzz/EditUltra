#ifndef _H_EDITULTRA_TABPAGES_
#define _H_EDITULTRA_TABPAGES_

#include "framework.h"

extern HWND	g_hwndTabPages ;
extern RECT	g_rectTabPages ;
extern HMENU	g_hTabPagePopupMenu ;
extern HMENU	g_hEditorPopupMenu ;
extern HMENU	g_hFunctionListPopupMenu ;
extern HMENU	g_hTreeViewPopupMenu ;

extern int	g_nTabsHeight ;

struct TabPage
{
	HWND				hwndScintilla ;
	SciFnDirect			pfuncScintilla ;
	sptr_t				pScintilla ;
	RECT				rectScintilla ;

	HWND				hwndSymbolList ;
	RECT				rectSymbolList ;

	pcre				*pcreSymbolRe ;

	HWND				hwndSymbolTree ;
	RECT				rectSymbolTree ;

	HWND				hwndQueryResultEdit ;
	RECT				rectQueryResultEdit ;
	HWND				hwndQueryResultTable ;
	RECT				rectQueryResultListView ;

	struct DatabaseConnectionConfig		stDatabaseConnectionConfig ;
	struct DatabaseConnectionHandles	stDatabaseConnectionHandles ;
	BOOL					bIsDatabaseConnected ;

	struct RedisConnectionConfig		stRedisConnectionConfig ;
	struct RedisConnectionHandles		stRedisConnectionHandles ;
	BOOL					bIsRedisConnected ;

	char				acPathFilename[ MAX_PATH ] ;
	char				acFilename[ MAX_PATH ] ;
	size_t				nFilenameLen ;
	char				acExtname[ _MAX_EXT ] ;
	time_t				st_mtime ;

	char				acEndOfLine[ 2+1 ] ;
	struct DocTypeConfig		*pstDocTypeConfig ;

	struct EncodingProbe		stEncodingProbe ;
	UINT				nCodePage ;

	struct NewLineProbe		stNewLineModeProbe ;

	struct RemoteFileServer		stRemoteFileServer ;
	size_t				nFileSize ;
	size_t				nWroteLen ;
};

extern struct TabPage			*g_pnodeCurrentTabPage ;

int CreateTabPages( HWND hWnd );

void AdjustTabPages();
void AdjustTabPageBox( struct TabPage *pnodeTabPage );

struct TabPage *AddTabPage( char *pcPathFilename , char *pcFilename , char *pcExtname );
int RemoveTabPage( struct TabPage *pnodeTabPage );
void SetTabPageTitle( int nTabPageNo , char *title );
void SelectTabPage( struct TabPage *pnodeTabPage , int nTabPageIndex );
void SelectTabPageByIndex( int nTabPageIndex );
void SetCurrentTabPageHighLight( int nCurrentTabPageIndex );

int OnSelectChange();
int OnFunctionListDbClick( struct TabPage *pnodeTabPage );

int CalcTabPagesHeight();

#define CONFIG_KEY_MATERIAL_TABPAGES		"TABPAG"

#endif

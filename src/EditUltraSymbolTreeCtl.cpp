#include "framework.h"

int CreateSymbolTreeCtl( struct TabPage *pnodeTabPage , HFONT hFont )
{
	if( pnodeTabPage->hwndSymbolTree )
	{
		DestroyWindow( pnodeTabPage->hwndSymbolTree );
	}

	/* 创建表列表框 */
	// pnodeTabPage->hwndSymbolTree = ::CreateWindow( WC_TREEVIEW , NULL , WS_BORDER|WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , 0 , 0 , 0 , 0 , g_hwndMainClient , NULL , g_hAppInstance , NULL ) ; 
	pnodeTabPage->hwndSymbolTree = ::CreateWindow( WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , 0 , 0 , 0 , 0 , g_hwndMainWindow , NULL , g_hAppInstance , NULL ) ; 
	if( pnodeTabPage->hwndSymbolTree == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建符号树控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	SendMessage( pnodeTabPage->hwndSymbolTree , WM_SETFONT , (WPARAM)hFont, 0);

	return 0;
}

int GetSymbolTreeItemAndAddTextToEditor( struct TabPage *pnodeTabPage )
{
	HTREEITEM	hti ;
	TVITEM		tvi ;
	char		acTableOrFieldName[ 256 ] ;
	char		*p = NULL ;
	BOOL		bret ;

	hti = TreeView_GetSelection( pnodeTabPage->hwndSymbolTree ) ;
	if( hti == NULL )
		return 1;

	memset( & tvi , 0x00 , sizeof(TVITEM) );
	tvi.mask = TVIF_HANDLE | TVIF_TEXT ;
	tvi.hItem = hti ;
	memset( acTableOrFieldName , 0x00 , sizeof(acTableOrFieldName) );
	tvi.cchTextMax = sizeof(acTableOrFieldName) - 1 ;
	tvi.pszText = acTableOrFieldName ;
	bret = TreeView_GetItem( pnodeTabPage->hwndSymbolTree , & tvi ) ;
	if( bret != TRUE )
		return -1;

	p = strchr( acTableOrFieldName , ' ' ) ;
	if( p )
		*(p) = '\0' ;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ADDTEXT , strlen(acTableOrFieldName) , (sptr_t)acTableOrFieldName ) ;

	return 0;
}

int ReloadSymbolTree_SQL( struct TabPage *pnodeTabPage )
{
	char		*acWords2 = NULL ;
	int		nWords2BufferLen ;
	int		nWords2BufferRemainLen ;
	int		nAddWord2Len ;

	char		sql[ 256 ] ;
	MYSQL_RES	*stMysqlResult = NULL ;
	MYSQL_RES	*stMysqlResult2 = NULL ;
	MYSQL_ROW	stMysqlRow ;
	MYSQL_ROW	stMysqlRow2 ;

	TVITEM		tvi ;
	TVINSERTSTRUCT	tvis ;
	HTREEITEM	htiRoot ;

	int		nret = 0 ;

	nret = ConnectToDatabase( pnodeTabPage ) ;
	if( nret < 0 )
	{
		return nret;
	}

	acWords2 = (char*)malloc( SQL_WORD2_BUFFER_SIZE ) ;
	if( acWords2 == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存用以存放表名字段名缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acWords2 , 0x00 , SQL_WORD2_BUFFER_SIZE );
	nWords2BufferLen = 0 ;
	nWords2BufferRemainLen = SQL_WORD2_BUFFER_SIZE - 1 ;

	if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "MySQL" ) == 0 )
	{
		struct MySqlFunctions	*pstMySqlFunctions = & (stDatabaseLibraryFunctions.stMysqlFunctions) ;
		struct MySqlHandles	*pstMySqlHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stMysqlHandles) ;

		memset( sql , 0x00 , sizeof(sql) );
		snprintf( sql , sizeof(sql)-1 , "SELECT table_name FROM information_schema.TABLES WHERE table_schema='%s'" , pnodeTabPage->stDatabaseConnectionConfig.dbname );
		nret = pstMySqlFunctions->pfunc_mysql_query( pstMySqlHandles->mysql , sql ) ;
		if( nret )
		{
			::MessageBox(NULL, TEXT("获取数据库表名列表失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( acWords2 );
			return -1;
		}

		stMysqlResult = pstMySqlFunctions->pfunc_mysql_store_result( pstMySqlHandles->mysql ) ;
		if( stMysqlResult )
		{
			TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

			while( stMysqlRow = pstMySqlFunctions->pfunc_mysql_fetch_row(stMysqlResult) )
			{
				memset( & tvi , 0x00 , sizeof(TVITEM) );
				memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
				tvi.mask = TVIF_TEXT ;
				tvi.pszText = stMysqlRow[0] ;
				tvis.hParent = TVI_ROOT ;
				tvis.hInsertAfter = TVI_LAST ;
				tvis.item = tvi;
				htiRoot = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

				nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , stMysqlRow[0] ) ;
				if( nAddWord2Len > 0 )
				{
					nWords2BufferLen += nAddWord2Len ;
					nWords2BufferRemainLen -= nAddWord2Len ;
				}

				memset( sql , 0x00 , sizeof(sql) );
				snprintf( sql , sizeof(sql)-1 , "SELECT column_name,data_type,character_maximum_length,numeric_precision,numeric_scale FROM information_schema.COLUMNS WHERE table_schema='%s' and table_name='%s' ORDER BY ordinal_position ASC" , pnodeTabPage->stDatabaseConnectionConfig.dbname , stMysqlRow[0] );
				nret = pstMySqlFunctions->pfunc_mysql_query( pstMySqlHandles->mysql , sql ) ;
				if( nret )
				{
					::MessageBox(NULL, TEXT("获取数据库字段名列表失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
					free( acWords2 );
					return -1;
				}

				stMysqlResult2 = pstMySqlFunctions->pfunc_mysql_store_result( pstMySqlHandles->mysql ) ;
				if( stMysqlResult2 )
				{
					while( stMysqlRow2 = pstMySqlFunctions->pfunc_mysql_fetch_row(stMysqlResult2) )
					{
						char	buf[ 1024 ] ;
						memset( & tvi , 0x00 , sizeof(TVITEM) );
						memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
						tvi.mask = TVIF_TEXT ;
						memset( buf , 0x00 , sizeof(buf) );
						if( stMysqlRow2[2] )
							snprintf( buf , sizeof(buf)-1 , "%s %s(%s)" , stMysqlRow2[0] , stMysqlRow2[1] , stMysqlRow2[2] );
						else if( stMysqlRow2[3] && stMysqlRow2[4] )
							snprintf( buf , sizeof(buf)-1 , "%s %s(%s,%s)" , stMysqlRow2[0] , stMysqlRow2[1] , stMysqlRow2[3] , stMysqlRow2[4] );
						else
							snprintf( buf , sizeof(buf)-1 , "%s %s" , stMysqlRow2[0] , stMysqlRow2[1] );
						tvi.pszText = buf ;
						tvis.hParent = htiRoot ;
						tvis.hInsertAfter = TVI_LAST ;
						tvis.item = tvi;
						TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis );

						nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , stMysqlRow2[0] ) ;
						if( nAddWord2Len > 0 )
						{
							nWords2BufferLen += nAddWord2Len ;
							nWords2BufferRemainLen -= nAddWord2Len ;
						}
					}
				}

				pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult2 );
			}
		}

		pstMySqlFunctions->pfunc_mysql_free_result( stMysqlResult );
	}
	else if( _stricmp( pnodeTabPage->stDatabaseConnectionConfig.dbtype , "Oracle" ) == 0 )
	{
		struct OracleFunctions	*pstOracleFunctions = & (stDatabaseLibraryFunctions.stOracleFunctions) ;
		struct OracleHandles	*pstOracleHandles = & (pnodeTabPage->stDatabaseConnectionHandles.handles.stOracleHandles) ;
		sword			swResult ;

		OCIStmt		*stmthpp = NULL ;
		OCIStmt		*stmthpp2 = NULL ;

		OCIDefine	*ocid_table_name = NULL ;
		OCIDefine	*ocid_column_name = NULL ;
		OCIDefine	*ocid_data_type = NULL ;
		OCIDefine	*ocid_data_length = NULL ;
		OCIDefine	*ocid_data_precision = NULL ;
		OCIDefine	*ocid_data_scale = NULL ;

		char		table_name[ 128 + 1 ] ;
		char		column_name[ 128 + 1 ] ;
		char		data_type[ 128 + 1 ] ;
		int		data_length = 0 ;
		int		data_precision = 0 ;
		int		data_scale = 0 ;

		ub2		table_name_len ;
		ub2		column_name_len ;
		ub2		data_type_len ;
		ub2		data_length_len ;
		ub2		data_precision_len ;
		ub2		data_scale_len ;

		sb2		table_name_indicator = 0 ;
		sb2		column_name_indicator = 0 ;
		sb2		data_type_indicator = 0 ;
		sb2		data_length_indicator = 0 ;
		sb2		data_precision_indicator = 0 ;
		sb2		data_scale_indicator = 0 ;

		char sql[ 1024 ] = "SELECT table_name FROM user_tables" ;

		pstOracleFunctions->pfuncOCIHandleAlloc( (dvoid *)(pstOracleHandles->envhpp) , (dvoid **) & stmthpp , OCI_HTYPE_STMT , (size_t)0 , (dvoid **)0 );
		swResult = pstOracleFunctions->pfuncOCIStmtPrepare( stmthpp , pstOracleHandles->errhpp , (text *)sql , (ub4)strlen(sql) , (ub4)OCI_NTV_SYNTAX , (ub4)OCI_DEFAULT ) ;
		if( swResult != OCI_SUCCESS )
		{
			int	nErrorCode ;
			char	acErrorDesc[ 512 ] = "" ;
			GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"构造查询用户表列表失败[%d][%s]" , nErrorCode , acErrorDesc );
			return -1;
		}

		pstOracleFunctions->pfuncOCIDefineByPos( stmthpp , & ocid_table_name , pstOracleHandles->errhpp, 1, (dvoid *)table_name , sizeof(table_name)-1 , SQLT_STR , (void*)table_name_indicator , & table_name_len , NULL , OCI_DEFAULT );

		swResult = pstOracleFunctions->pfuncOCIStmtExecute( pstOracleHandles->svchpp , stmthpp , pstOracleHandles->errhpp , (ub4)1 , (ub4)0 , (OCISnapshot *)NULL , (OCISnapshot *)NULL , OCI_DEFAULT ) ;
		if( swResult != OCI_SUCCESS )
		{
			int	nErrorCode ;
			char	acErrorDesc[ 512 ] = "" ;
			GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
			AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行查询用户表列表失败[%d][%s]" , nErrorCode , acErrorDesc );
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		do
		{
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
			tvi.mask = TVIF_TEXT ;
			tvi.pszText = table_name ;
			tvis.hParent = TVI_ROOT ;
			tvis.hInsertAfter = TVI_LAST ;
			tvis.item = tvi;
			htiRoot = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

			nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , table_name ) ;
			if( nAddWord2Len > 0 )
			{
				nWords2BufferLen += nAddWord2Len ;
				nWords2BufferRemainLen -= nAddWord2Len ;
			}

			memset( sql , 0x00 , sizeof(sql) );
			snprintf( sql , sizeof(sql)-1 , "SELECT column_name,data_type,data_length,data_precision,data_scale FROM user_tab_columns WHERE table_name='%s' ORDER BY column_id ASC" , table_name );

			pstOracleFunctions->pfuncOCIHandleAlloc( (dvoid *)(pstOracleHandles->envhpp) , (dvoid **) & stmthpp2 , OCI_HTYPE_STMT , (size_t)0 , (dvoid **)0 );
			swResult = pstOracleFunctions->pfuncOCIStmtPrepare( stmthpp2 , pstOracleHandles->errhpp , (text *)sql , (ub4)strlen(sql) , (ub4)OCI_NTV_SYNTAX , (ub4)OCI_DEFAULT ) ;
			if( swResult != OCI_SUCCESS )
			{
				int	nErrorCode ;
				char	acErrorDesc[ 512 ] = "" ;
				GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"构造查询用户表字段列表失败[%d][%s]" , nErrorCode , acErrorDesc );
				return -1;
			}

			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_column_name , pstOracleHandles->errhpp, 1, (dvoid *)column_name , sizeof(column_name)-1 , SQLT_STR , (void*)&column_name_indicator , & column_name_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_type , pstOracleHandles->errhpp, 2, (dvoid *)data_type , sizeof(data_type)-1 , SQLT_STR , (void*)&data_type_indicator , & data_type_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_length , pstOracleHandles->errhpp, 3, (dvoid *)&data_length , sizeof(data_length) , SQLT_INT , (void*)&data_length_indicator , & data_length_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_precision , pstOracleHandles->errhpp, 4, (dvoid *)&data_precision , sizeof(data_precision) , SQLT_INT , (void*)&data_precision_indicator , & data_precision_len , NULL , OCI_DEFAULT );
			pstOracleFunctions->pfuncOCIDefineByPos( stmthpp2 , & ocid_data_scale , pstOracleHandles->errhpp, 5, (dvoid *)&data_scale , sizeof(data_scale) , SQLT_INT , (void*)&data_scale_indicator , & data_scale_len , NULL , OCI_DEFAULT );

			swResult = pstOracleFunctions->pfuncOCIStmtExecute( pstOracleHandles->svchpp , stmthpp2 , pstOracleHandles->errhpp , (ub4)1 , (ub4)0 , (OCISnapshot *)NULL , (OCISnapshot *)NULL , OCI_DEFAULT ) ;
			if( swResult != OCI_SUCCESS )
			{
				int	nErrorCode ;
				char	acErrorDesc[ 512 ] = "" ;
				GetOracleErrCode( pnodeTabPage , pstOracleHandles->errhpp , & nErrorCode , acErrorDesc , sizeof(acErrorDesc)-1 );
				AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"执行查询用户表字段列表失败[%d][%s]" , nErrorCode , acErrorDesc );
				return -1;
			}

			do
			{
				char	buf[ 1024 ] ;
				memset( & tvi , 0x00 , sizeof(TVITEM) );
				memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
				tvi.mask = TVIF_TEXT ;
				memset( buf , 0x00 , sizeof(buf) );
				if( data_precision_indicator == -1 )
					snprintf( buf , sizeof(buf)-1 , "%s %s(%d)" , column_name , data_type , data_length );
				else
					snprintf( buf , sizeof(buf)-1 , "%s %s(%d,%d)" , column_name , data_type , data_precision , data_scale );
				tvi.pszText = buf ;
				tvis.hParent = htiRoot ;
				tvis.hInsertAfter = TVI_LAST ;
				tvis.item = tvi;
				TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis );

				nAddWord2Len = snprintf( acWords2+nWords2BufferLen , nWords2BufferRemainLen , "%s " , column_name ) ;
				if( nAddWord2Len > 0 )
				{
					nWords2BufferLen += nAddWord2Len ;
					nWords2BufferRemainLen -= nAddWord2Len ;
				}
			}
			while( pstOracleFunctions->pfuncOCIStmtFetch2( stmthpp2 , pstOracleHandles->errhpp , 1 , OCI_FETCH_NEXT , 1 , OCI_DEFAULT ) != OCI_NO_DATA );

			pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp2 , OCI_HTYPE_STMT );
		}
		while( pstOracleFunctions->pfuncOCIStmtFetch2( stmthpp , pstOracleHandles->errhpp , 1 , OCI_FETCH_NEXT , 1 , OCI_DEFAULT ) != OCI_NO_DATA );

		pstOracleFunctions->pfuncOCIHandleFree( (dvoid *)stmthpp , OCI_HTYPE_STMT );
	}
	else
	{
		AppendSqlQueryResultEditText( pnodeTabPage->hwndQueryResultEdit , (char*)"数据库类型[%s]暂不支持" , pnodeTabPage->stDatabaseConnectionConfig.dbtype );
		return 0;
	}

	DestroyAutoCompletedShowTree( & (pnodeTabPage->pstDocTypeConfig->stAutoCompletedShowTree) );
	pnodeTabPage->pstDocTypeConfig->sAutoCompletedBufferSize = 0 ;

	nret = BuildAutoCompletedShowTree( pnodeTabPage->pstDocTypeConfig , pnodeTabPage->pstDocTypeConfig->autocomplete_set ) ;
	if( nret )
		return nret;

	nret = BuildAutoCompletedShowTree( pnodeTabPage->pstDocTypeConfig , acWords2 ) ;
	if( nret )
		return nret;

	nret = ExpandAutoCompletedShowTreeToBuffer( pnodeTabPage->pstDocTypeConfig ) ;
	if( nret )
		return nret;

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)acWords2 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETFORE , SCE_C_WORD2 , (sptr_t)(g_stStyleTheme.keywords2.color) );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD2 , (sptr_t)(g_stStyleTheme.keywords2.bold) );

	free( acWords2 );

	return 0;
}

static HTREEITEM AddReplyToSymbolTree_REDIS( struct TabPage *pnodeTabPage , HTREEITEM hTreeItem , struct redisReply *reply )
{
	char		buf[ 1024 ] ;

	TVITEM		tvi ;
	TVINSERTSTRUCT	tvis ;
	HTREEITEM	hti ;

	HTREEITEM	hret ;

	memset( buf , 0x00 , sizeof(buf) );
	if( reply->type == REDIS_REPLY_STATUS )
		snprintf( buf , sizeof(buf)-1 , "%s (STATUS)" , reply->str );
	else if( reply->type == REDIS_REPLY_ERROR )
		snprintf( buf , sizeof(buf)-1 , "%s (ERROR)" , reply->str );
	else if( reply->type == REDIS_REPLY_STRING )
		snprintf( buf , sizeof(buf)-1 , "%.*s (STRING)" , reply->len , reply->str );
	else if( reply->type == REDIS_REPLY_INTEGER )
		snprintf( buf , sizeof(buf)-1 , "%lld (ERROR)" , reply->integer );
	else if( reply->type == REDIS_REPLY_NIL )
		snprintf( buf , sizeof(buf)-1 , "(null) (NIL)" );
	else if( reply->type == REDIS_REPLY_ARRAY )
		snprintf( buf , sizeof(buf)-1 , "%d (ELEMENTS)" , reply->elements );

	memset( & tvi , 0x00 , sizeof(TVITEM) );
	memset( & tvis , 0x00 , sizeof(TVINSERTSTRUCT) );
	tvi.mask = TVIF_TEXT ;
	tvi.pszText = buf ;
	tvis.hParent = hTreeItem ;
	tvis.hInsertAfter = TVI_LAST ;
	tvis.item = tvi;
	hti = TreeView_InsertItem( pnodeTabPage->hwndSymbolTree , & tvis ) ;

	if( reply->type == REDIS_REPLY_ARRAY )
	{
		size_t	i ;

		for( i = 0 ; i < reply->elements ; i++ )
		{
			hret = AddReplyToSymbolTree_REDIS( pnodeTabPage , hti , reply->element[i] ) ;
			if( hret == NULL )
				return hret;
		}
	}

	return hti;
}

int ConnectToRedis( struct TabPage *pnodeTabPage )
{
	int		nret = 0 ;
	HTREEITEM	hret ;
	
	if( pnodeTabPage->stRedisConnectionConfig.host[0] == '\0' )
		return 1;

	stRedisLibraryFunctions ;
	pnodeTabPage->stRedisConnectionHandles ;

	if( stRedisLibraryFunctions.hmod_hiredis_dll == NULL )
	{
		stRedisLibraryFunctions.hmod_hiredis_dll = ::LoadLibrary( "hiredis.dll" ) ;
		if( stRedisLibraryFunctions.hmod_hiredis_dll == NULL )
		{
			::MessageBox(NULL, TEXT("不能装载hiredis.dll，请检查是否已安装hiredis以及系统环境变量PATH是否包含hiredis动态链接库文件目录"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		stRedisLibraryFunctions.pfuncRedisConnectWithTimeout = (funcRedisConnectWithTimeout *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "redisConnectWithTimeout" ) ;
		stRedisLibraryFunctions.pfuncRedisFree = (funcRedisFree *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "redisFree" ) ;
		stRedisLibraryFunctions.pfuncRedisCommand = (funcRedisCommand *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "redisCommand" ) ;
		stRedisLibraryFunctions.pfuncFreeReplyObject = (funcFreeReplyObject *)::GetProcAddress( stRedisLibraryFunctions.hmod_hiredis_dll , "freeReplyObject" ) ;
		if(	stRedisLibraryFunctions.pfuncRedisConnectWithTimeout == NULL
			|| stRedisLibraryFunctions.pfuncRedisFree == NULL
			|| stRedisLibraryFunctions.pfuncRedisCommand == NULL
			|| stRedisLibraryFunctions.pfuncFreeReplyObject == NULL
			)
		{
			::MessageBox(NULL, TEXT("不能定位函数符号在hiredis.dll"), TEXT("错误"), MB_ICONERROR | MB_OK);
			FreeLibrary( stRedisLibraryFunctions.hmod_hiredis_dll );
			return -1;
		}
	}

	if( pnodeTabPage->stRedisConnectionHandles.ctx == NULL )
	{
		struct timeval		timeout ;
		char			command[ 256 ] ;
		struct redisReply	*reply = NULL ;

		timeout.tv_sec = 10 ;
		timeout.tv_usec = 0 ;
		pnodeTabPage->stRedisConnectionHandles.ctx = stRedisLibraryFunctions.pfuncRedisConnectWithTimeout( pnodeTabPage->stRedisConnectionConfig.host , pnodeTabPage->stRedisConnectionConfig.port , timeout ) ;
		if( pnodeTabPage->stRedisConnectionHandles.ctx == NULL || pnodeTabPage->stRedisConnectionHandles.ctx->err )
		{
			ConfirmErrorInfo( "不能连接Redis服务器[%s:%d]" , pnodeTabPage->stRedisConnectionConfig.host , pnodeTabPage->stRedisConnectionConfig.port );
			if( pnodeTabPage->stRedisConnectionHandles.ctx )
			{
				stRedisLibraryFunctions.pfuncRedisFree( pnodeTabPage->stRedisConnectionHandles.ctx ); pnodeTabPage->stRedisConnectionHandles.ctx = NULL ;
			}
			return -1;
		}

		if( pnodeTabPage->stRedisConnectionConfig.pass[0] )
		{
			memset( command , 0x00 , sizeof(command) );
			snprintf( command , sizeof(command)-1 , "AUTH %s" , pnodeTabPage->stRedisConnectionConfig.pass );
			reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , command ) ;
			if( reply == NULL || reply->type == REDIS_REPLY_ERROR )
			{
				ConfirmErrorInfo( "向Redis服务器认证失败，请确认密码正确性" );
				if( pnodeTabPage->stRedisConnectionHandles.ctx )
				{
					stRedisLibraryFunctions.pfuncRedisFree( pnodeTabPage->stRedisConnectionHandles.ctx ); pnodeTabPage->stRedisConnectionHandles.ctx = NULL ;
				}
				if( reply )
				{
					stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
				}
				return -1;
			}

			stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		}

		if( pnodeTabPage->stRedisConnectionConfig.dbsl[0] )
		{
			memset( command , 0x00 , sizeof(command) );
			snprintf( command , sizeof(command)-1 , "SELECT %s" , pnodeTabPage->stRedisConnectionConfig.dbsl );
			reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , command ) ;
			if( reply == NULL || reply->type == REDIS_REPLY_ERROR )
			{
				ConfirmErrorInfo( "切换Redis服务器数据桶失败，请确认数据桶配置值正确性" );
				if( pnodeTabPage->stRedisConnectionHandles.ctx )
				{
					stRedisLibraryFunctions.pfuncRedisFree( pnodeTabPage->stRedisConnectionHandles.ctx ); pnodeTabPage->stRedisConnectionHandles.ctx = NULL ;
				}
				if( reply )
				{
					stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
				}
				return -1;
			}

			stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		}

		reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , "PING" ) ;
		if( reply == NULL || reply->type == REDIS_REPLY_ERROR )
		{
			ConfirmErrorInfo( "向Redis服务器发送PING失败" );
			if( pnodeTabPage->stRedisConnectionHandles.ctx )
			{
				stRedisLibraryFunctions.pfuncRedisFree( pnodeTabPage->stRedisConnectionHandles.ctx ); pnodeTabPage->stRedisConnectionHandles.ctx = NULL ;
			}
			if( reply )
			{
				stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
			}
			return -1;
		}

		TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

		hret = AddReplyToSymbolTree_REDIS( pnodeTabPage , TVI_ROOT , reply ) ;
		stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
		if( hret == NULL )
			return -1;

		TreeView_Expand( pnodeTabPage->hwndSymbolTree , hret , TVE_EXPAND );


		return 1;
	}

	return 0;
}

int ExecuteRedisQuery_REDIS( struct TabPage *pnodeTabPage )
{
	int			nSelStartPos ;
	int			nSelEndPos ;
	int			nSelSqlLength ;
	char			*acSelCommand = NULL ;

	struct redisReply	*reply = NULL ;

	HTREEITEM		hret ;
	int			nret = 0 ;

	nret = ConnectToRedis( pnodeTabPage ) ;
	if( nret < 0 )
	{
		return nret;
	}

	nSelStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	nSelEndPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	nSelSqlLength = nSelEndPos - nSelStartPos ;
	if( nSelSqlLength <= 0 )
		return 0;

	acSelCommand = (char*)malloc( nSelSqlLength+1 ) ;
	if( acSelCommand == NULL )
	{
		::MessageBox(NULL, TEXT("不能分配内存以存放SQL"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acSelCommand , 0x00 , nSelSqlLength+1 );
	GetTextByRange( pnodeTabPage , nSelStartPos , nSelEndPos , acSelCommand );

	reply = (struct redisReply *)stRedisLibraryFunctions.pfuncRedisCommand( pnodeTabPage->stRedisConnectionHandles.ctx , acSelCommand ) ;
	free( acSelCommand );
	if( reply == NULL )
	{
		stRedisLibraryFunctions.pfuncRedisFree( pnodeTabPage->stRedisConnectionHandles.ctx ); pnodeTabPage->stRedisConnectionHandles.ctx = NULL ;
		return -1;
	}

	TreeView_DeleteAllItems( pnodeTabPage->hwndSymbolTree );

	hret = AddReplyToSymbolTree_REDIS( pnodeTabPage , TVI_ROOT , reply ) ;
	stRedisLibraryFunctions.pfuncFreeReplyObject( reply );
	if( hret == NULL )
		return -1;

	TreeView_Expand( pnodeTabPage->hwndSymbolTree , hret , TVE_EXPAND );

	return 0;
}

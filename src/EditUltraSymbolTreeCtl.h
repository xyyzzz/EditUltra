#ifndef _H_EDITULTRA_SYMBOLTREECTL_
#define _H_EDITULTRA_SYMBOLTREECTL_

#include "framework.h"

int CreateSymbolTreeCtl( struct TabPage *pnodeTabPage , HFONT hFont );
int GetSymbolTreeItemAndAddTextToEditor( struct TabPage *pnodeTabPage );

/*
 * SQL
 */

int ReloadSymbolTree_SQL( struct TabPage *pnodeTabPage );

/*
 * Redis
 */

typedef redisContext *funcRedisConnectWithTimeout(const char *ip, int port, const struct timeval tv);
typedef void funcRedisFree(redisContext *c);
typedef void *funcRedisCommand(redisContext *c, const char *format, ...);
typedef void funcFreeReplyObject(void *reply);

struct RedisLibraryFunctions
{
	HMODULE				hmod_hiredis_dll ;
	funcRedisConnectWithTimeout	*pfuncRedisConnectWithTimeout ;
	funcRedisFree			*pfuncRedisFree ;
	funcRedisCommand		*pfuncRedisCommand ;
	funcFreeReplyObject		*pfuncFreeReplyObject ;
};

extern struct RedisLibraryFunctions	stRedisLibraryFunctions ;

struct RedisConnectionHandles
{
	redisContext		*ctx ;
};

struct RedisConnectionConfig
{
	char	host[ 40 ] ;
	int	port ;
	char	pass[ 64 ] ;
	char	dbsl[ 20 ] ;
};

int ParseRedisFileConfigHeader( struct TabPage *pnodeTabPage );

int ConnectToRedis( struct TabPage *pnodeTabPage );
int ExecuteRedisQuery_REDIS( struct TabPage *pnodeTabPage );

#endif

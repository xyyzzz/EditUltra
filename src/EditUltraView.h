#ifndef _H_EDITULTRA_SOURCECODE_
#define _H_EDITULTRA_SOURCECODE_

#include "framework.h"

int OnViewFileTree( struct TabPage *pnodeTabPage );

int OnViewStyleTheme( struct TabPage *pnodeTabPage );

int OnViewTabWidth( struct TabPage *pnodeTabPage );
int OnViewWrapLineMode( struct TabPage *pnodeTabPage );

int OnViewLineNumberVisiable( struct TabPage *pnodeTabPage );
int OnViewBookmarkVisiable( struct TabPage *pnodeTabPage );

int OnViewWhiteSpaceVisiable( struct TabPage *pnodeTabPage );
int OnViewNewLineVisiable( struct TabPage *pnodeTabPage );
int OnViewIndentationGuidesVisiable( struct TabPage *pnodeTabPage );

int OnViewZoomOut( struct TabPage *pnodeTabPage );
int OnViewZoomIn( struct TabPage *pnodeTabPage );
int OnViewZoomReset( struct TabPage *pnodeTabPage );

#endif

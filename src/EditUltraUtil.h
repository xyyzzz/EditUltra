#ifndef _H_EDITULTRA_UTIL_
#define _H_EDITULTRA_UTIL_

#include "framework.h"

void GenarateConfigKey();
int AesEncrypt( unsigned char *dec , unsigned char *enc , int len );
int AesDecrypt( unsigned char *enc , unsigned char *dec , int len );

int Encrypt_3DES_ECB_192bits( unsigned char *key_192bits
	, unsigned char *decrypt , long decrypt_len
	, unsigned char *encrypt , long *encrypt_len );
int Decrypt_3DES_ECB_192bits( unsigned char *key_192bits
	, unsigned char *encrypt , long encrypt_len
	, unsigned char *decrypt , long *decrypt_len );
int Encrypt_3DES_CBC_192bits( unsigned char *key_192bits
	, unsigned char *decrypt , long decrypt_len
	, unsigned char *encrypt , long *encrypt_len
	, unsigned char *init_vector );
int Decrypt_3DES_CBC_192bits( unsigned char *key_192bits
	, unsigned char *encrypt , long encrypt_len
	, unsigned char *decrypt , long *decrypt_len
	, unsigned char *init_vector );

int HexExpand( char *HexBuf , int HexBufLen , char *AscBuf );
int HexFold( char *AscBuf , int AscBufLen , char *HexBuf );

enum FileType
{
	FILETYPE_ERROR = -1 ,
	FILETYPE_REGULAR = 1 ,
	FILETYPE_DIRECTORY = 2 ,
	FILETYPE_OTHER = 3
};

enum FileType GetFileType( char *acPathFileName );

char **CommandLineToArgvA( LPWSTR lpCmdLine, int *_argc );

int SetWindowTitle( char* filename );
BOOL CenterWindow(HWND hWnd, HWND hParent);
char *StrdupEditorSelection( size_t *p_nTextLen , size_t multiple );
void CopyEditorSelectionToWnd( HWND hwnd );

void SetMenuItemEnable(HWND hWnd, int nMenuItemId, bool enable);
void SetMenuItemChecked(HWND hWnd, int nMenuItemId, bool checked);

void ConfirmErrorInfo( const char *format , ... );

char* ConvertEncodingEx( const char *encFrom , const char *encTo , const char *p_ori_in , size_t ori_in_len , char *p_ori_out , size_t *p_ori_out_size );

void FoldNewLineString( char *str );

void ToUpperString( char *str );

#endif

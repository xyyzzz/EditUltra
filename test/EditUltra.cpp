﻿// EditUltra.cpp : 定义应用程序的入口点。
//

#include "framework.h"

#define MAX_LOADSTRING 100

struct EditUltraMainConfig		g_stEditUltraMainConfig ;

// 全局变量:
char		**g_argv = NULL ;
int		g_argc ;
HINSTANCE	g_hAppInstance = NULL ;				// 当前实例
HWND		g_hwndMainWindow = NULL ;			// 当前窗口
char		g_acAppName[100];
char		g_acWindowClassName[100];
WCHAR		g_szTitle[MAX_LOADSTRING];			// 标题栏文本
WCHAR		g_szWindowClass[MAX_LOADSTRING];		// 主窗口类名

char		g_acModuleFileName[ MAX_PATH ] ;
char		g_acModulePathName[ MAX_PATH ] ;

int		g_nZoomReset = 0 ;

BOOL		g_bIsFileTreeBarHoverResizing = FALSE ;
BOOL		g_bIsFileTreeBarResizing = FALSE ;
BOOL		g_bIsTabPageMoving = FALSE ;
int		g_nTabPageMoveFrom = 0 ;
POINT		g_rectLMouseDown = { 0 } ;
BOOL		g_bIsFunctionListHoverResizing = FALSE ;
BOOL		g_bIsFunctionListResizing = FALSE ;
BOOL		g_bIsTreeViewHoverResizing = FALSE ;
BOOL		g_bIsTreeViewResizing = FALSE ;
BOOL		g_bIsSqlQueryResultEditHoverResizing = FALSE ;
BOOL		g_bIsSqlQueryResultEditResizing = FALSE ;
BOOL		g_bIsSqlQueryResultListViewHoverResizing = FALSE ;
BOOL		g_bIsSqlQueryResultListViewResizing = FALSE ;

// 此代码模块中包含的函数的前向声明:
ATOM			MyRegisterClass(HINSTANCE hInstance);
BOOL			InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
					 _In_opt_ HINSTANCE hPrevInstance,
					 _In_ LPWSTR	lpCmdLine,
					 _In_ int	nCmdShow)
{
	char	acCurrentDirectory[ MAX_PATH ] ;

	int	nret = 0 ;

	memset( acCurrentDirectory , 0x00 , sizeof(acCurrentDirectory) );
	::GetCurrentDirectory( sizeof(acCurrentDirectory)-1 , acCurrentDirectory );
	SetLogcFile( (char*)"%s\\log\\EditUltra.log" , acCurrentDirectory );
	SetLogcLevel( LOGCLEVEL_NOLOG );
	DEBUGLOGC( "--- EditUltra Start Debugging ---" )

	UNREFERENCED_PARAMETER(hPrevInstance);
	g_argv = CommandLineToArgvA( lpCmdLine , & g_argc ) ;

	// 初始化全局字符串
	LoadStringA(hInstance, IDS_APP_TITLE, g_acAppName, 100);
	LoadStringA(hInstance, IDC_EDITULTRA, g_acWindowClassName, 100);
	LoadStringW(hInstance, IDS_APP_TITLE, g_szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_EDITULTRA, g_szWindowClass, MAX_LOADSTRING);

	{
		memset( g_acModuleFileName , 0x00 , sizeof(g_acModuleFileName) );
		::GetModuleFileName( NULL , g_acModuleFileName , sizeof(g_acModuleFileName) );
		strcpy( g_acModulePathName , g_acModuleFileName );
		char *p = strrchr( g_acModulePathName , '\\' ) ;
		if( p )
			*(p) = '\0' ;
	}

	if( g_argc == 1 && g_argv[0] )
	{
		HWND hwnd = ::FindWindow( g_acWindowClassName , NULL ) ;
		if( hwnd )
		{
			COPYDATASTRUCT	cpd ;

			memset( & cpd , 0x00 , sizeof(COPYDATASTRUCT) );
			cpd.lpData = g_argv[0] ;
			cpd.cbData = (DWORD)strlen(g_argv[0]) ;
			::SendMessage( hwnd , WM_COPYDATA , 0 , (LPARAM)&cpd );
			::SendMessage( hwnd , WM_ACTIVATE , 0 , 0 );

			return 0;
		}
	}

	SetEditUltraMainConfigDefault( & g_stEditUltraMainConfig );

	SetStyleThemeDefault( & g_stStyleTheme );

	INIT_LIST_HEAD( & listRemoteFileServer );

	InitNavigateBackNextTraceList();

	// TODO: 在此处放置代码。
	LoadConfig();

	// 装载Scintilla控件
	HMODULE hmod = ::LoadLibrary(TEXT("SciLexer.DLL")) ;
	if (hmod == NULL)
	{
		::MessageBox(NULL, TEXT("不能装载Scintilla控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return 1;
	}

	curl_global_init( CURL_GLOBAL_DEFAULT );

	// 执行应用程序初始化:
	MyRegisterClass(hInstance);
	if( ! InitInstance (hInstance, nCmdShow) )
	{
		return FALSE;
	}

#if 0
	{
		char	acInputBuf[ 256 ] ;
		memset( acInputBuf , 0x00 , sizeof(acInputBuf) );
		nret = InputBox( hwndMainClient , "请输入：" , "输入窗口" , 0 , acInputBuf , sizeof(acInputBuf)-1 ) ;
		if( nret == IDOK )
		{
			::MessageBox( NULL , acInputBuf , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		else if( nret == IDCANCEL )
		{
			::MessageBox( NULL , "输入窗口被取消" , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		else
		{
			::MessageBox( NULL , "输入窗口返回错误" , TEXT("测试输入窗口") , MB_ICONERROR | MB_OK );
		}
		// exit(0);
	}
#endif

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_EDITULTRA));

#if 0
char buf[1024];
memset(buf,0x00,sizeof(buf));
snprintf(buf,sizeof(buf)-1,"argc[%d]",argc);
::MessageBox(NULL, TEXT(buf), TEXT("错误"), MB_ICONERROR | MB_OK);
for(int i=0;i<argc;i++)
{
snprintf(buf,sizeof(buf)-1,"argv[%d][%s]",i,argv[i]);
::MessageBox(NULL, TEXT(buf), TEXT("错误"), MB_ICONERROR | MB_OK);
}
exit(0);
#endif
	
	if( g_argc == 1 && g_argv[0] )
	{
		OpenFileOrDirectoryDirectly( g_argv[0] );
	}

	MSG msg;

	// 主消息循环:
	while( GetMessage( & msg , nullptr , 0 , 0 ) )
	{
		if ( !IsDialogMessage( hwndSearchFind, &msg ) && !IsDialogMessage( hwndSearchReplace, & msg ) )
		{
			nret = BeforeWndProc( & msg ) ;
			if( nret > 0 )
				continue;

			if( ! TranslateAccelerator( g_hwndMainWindow , hAccelTable , & msg ) )
			{
				TranslateMessage( & msg );

				DispatchMessage( & msg );
			}

			nret = AfterWndProc( & msg ) ;
			if( nret > 0 )
				continue;
		}
	}

	SaveMainConfigFile();
	SaveStyleThemeConfigFile();

	return (int) msg.wParam;
}

//
//  函数: MyRegisterClass()
//
//  目标: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style		= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon		= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_EDITULTRA));
	wcex.hCursor		= LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName	= MAKEINTRESOURCEA(IDC_EDITULTRA);
	wcex.lpszClassName	= g_acWindowClassName;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return ::RegisterClassEx(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目标: 保存实例句柄并创建主窗口
//
//   注释:
//
//		在此函数中，我们在全局变量中保存实例句柄并
//		创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	g_hAppInstance = hInstance; // 将实例句柄存储在全局变量中
   
	g_hwndMainWindow = ::CreateWindowEx( WS_EX_ACCEPTFILES, g_acWindowClassName, g_acAppName, WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_CLIPCHILDREN, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
	if (!g_hwndMainWindow)
	{
		return FALSE;
	}

	hwndSearchFind = CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_FIND_DIALOG), g_hwndMainWindow , SearchFindWndProc ) ;
	if (!hwndSearchFind)
	{
		return FALSE;
	}
	::CheckRadioButton( hwndSearchFind , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	::CheckRadioButton( hwndSearchFind , IDC_AREATYPE_THISFILE , IDC_AREATYPE_THISFILE , IDC_AREATYPE_THISFILE );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WHOLEWORD , true );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_MATCHCASE , true );
	hwndEditBoxInSearchFind = GetDlgItem( hwndSearchFind , IDC_SEARCH_TEXT ) ;
	ShowWindow( hwndSearchFind , SW_HIDE );

	hwndSearchReplace = CreateDialog( hInstance ,MAKEINTRESOURCE(IDD_SEARCH_REPLACE_DIALOG), g_hwndMainWindow , SearchReplaceWndProc ) ;
	if (!hwndSearchReplace)
	{
		return FALSE;
	}
	::CheckRadioButton( hwndSearchReplace , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	::CheckRadioButton( hwndSearchReplace , IDC_AREATYPE_THISFILE , IDC_AREATYPE_THISFILE , IDC_AREATYPE_THISFILE );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_WHOLEWORD , true );
	::CheckDlgButton( hwndSearchReplace , IDC_OPTIONS_MATCHCASE , true );
	hwndFromEditBoxInSearchReplace = GetDlgItem( hwndSearchReplace , IDC_SEARCH_FROMTEXT ) ;
	hwndToEditBoxInSearchReplace = GetDlgItem( hwndSearchReplace , IDC_SEARCH_TOTEXT ) ;
	ShowWindow( hwndSearchReplace , SW_HIDE );

	UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );

	ShowWindow(g_hwndMainWindow, SW_SHOWMAXIMIZED);
	UpdateWindow(g_hwndMainWindow);

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目标: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT	- 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//

static int OnCreateWindow( HWND hWnd , WPARAM wParam , LPARAM lParam )
{
	BOOL	bret ;
	int	nret ;
	LSTATUS	lsret ;
	HKEY	regkey_EditUltra ;

	//// ::EnableMenuItem(::GetSubMenu(::GetMenu(hwndMainClient),3), 0, MF_DISABLED | MF_GRAYED | MF_BYCOMMAND);

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_TAB_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE)
	{
		::MessageBox(NULL, TEXT("不能注册TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nret = CreateFileTreeBar( hWnd ) ;
	if( nret )
		return nret;

	nret = CreateTabPages( hWnd ) ;
	if( nret )
		return nret;

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra" , & regkey_EditUltra ) ;
	if( lsret == ERROR_SUCCESS )
	{
		SetMenuItemChecked( hWnd, IDM_ENV_FILE_POPUPMENU, true);

		g_bIsEnvFilePopupMenuSelected = TRUE ;

		RegCloseKey( regkey_EditUltra );
	}
	else
	{
		SetMenuItemChecked( hWnd, IDM_ENV_FILE_POPUPMENU, false);

		g_bIsEnvFilePopupMenuSelected = FALSE ;
	}

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra" , & regkey_EditUltra ) ;
	if( lsret == ERROR_SUCCESS )
	{
		SetMenuItemChecked( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, true);

		g_bIsEnvDirectoryPopupMenuSelected = TRUE ;

		RegCloseKey( regkey_EditUltra );
	}
	else
	{
		SetMenuItemChecked( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, false);

		g_bIsEnvDirectoryPopupMenuSelected = FALSE ;
	}

	return 0;
}

void UpdateAllWindows()
{
	RECT	rectAdjust ;
	RECT	rectFileTree ;

	CalcTabPagesHeight();

	AdjustTabPages();

	::SetWindowPos ( hwndFileTreeBar , bIsFileTreeBarShow?HWND_TOP:0 ,  rectFileTreeBar.left , rectFileTreeBar.top , rectFileTreeBar.right-rectFileTreeBar.left , rectFileTreeBar.bottom-rectFileTreeBar.top , bIsFileTreeBarShow?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
	memcpy( & rectAdjust , & rectFileTreeBar , sizeof(RECT) );
	TabCtrl_AdjustRect( hwndFileTreeBar , FALSE , & rectAdjust );
	::ShowWindow( hwndFileTreeBar , (bIsFileTreeBarShow==TRUE?SW_SHOW:SW_HIDE) );
	/*
	if( bIsFileTreeBarShow == TRUE )
		::InvalidateRect( hwndFileTreeBar , NULL , TRUE );
	*/
	::UpdateWindow( hwndFileTreeBar );
	
	AdjustFileTreeBox( & rectFileTreeBar , & rectFileTree );

	::SetWindowPos( hwndFileTree , bIsFileTreeBarShow?HWND_TOP:0 , rectFileTree.left , rectFileTree.top , rectFileTree.right-rectFileTree.left , rectFileTree.bottom-rectFileTree.top , bIsFileTreeBarShow?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
	::ShowWindow( hwndFileTree , (bIsFileTreeBarShow==TRUE?SW_SHOW:SW_HIDE) );
	// if( bIsFileTreeBarShow == TRUE )
		::InvalidateRect( hwndFileTree , NULL , TRUE );
	::UpdateWindow( hwndFileTree );

	::SetWindowPos( g_hwndTabPages , HWND_TOP , g_rectTabPages.left , g_rectTabPages.top , g_rectTabPages.right-g_rectTabPages.left , g_rectTabPages.bottom-g_rectTabPages.top , SWP_SHOWWINDOW );
	memcpy( & rectAdjust , & g_rectTabPages , sizeof(RECT) );
	TabCtrl_AdjustRect( g_hwndTabPages , FALSE , & rectAdjust );
	::ShowWindow( g_hwndTabPages , SW_SHOW);
	::InvalidateRect( g_hwndTabPages , NULL , TRUE );
	::UpdateWindow( g_hwndTabPages );

	int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( int nTabPageIndex = 0 ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
	{
		TCITEM tci ;
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		struct TabPage *pnodeTabPage = (struct TabPage *)(tci.lParam);
		if( pnodeTabPage != g_pnodeCurrentTabPage )
		{
			::SetWindowPos( pnodeTabPage->hwndScintilla , 0 , pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.top , pnodeTabPage->rectScintilla.right-pnodeTabPage->rectScintilla.left , pnodeTabPage->rectScintilla.bottom-pnodeTabPage->rectScintilla.top , SWP_HIDEWINDOW );

			if( pnodeTabPage->hwndSymbolList )
			{
				::SetWindowPos( pnodeTabPage->hwndSymbolList , 0 , pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.top , pnodeTabPage->rectSymbolList.right-pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.bottom-pnodeTabPage->rectSymbolList.top , SWP_HIDEWINDOW );
				::ShowWindow( pnodeTabPage->hwndSymbolList , SW_HIDE);
				::InvalidateRect( pnodeTabPage->hwndSymbolList , NULL , TRUE );
				::UpdateWindow( pnodeTabPage->hwndSymbolList );
			}

			if( pnodeTabPage->hwndSymbolTree )
			{
				::SetWindowPos( pnodeTabPage->hwndSymbolTree , 0 , pnodeTabPage->rectSymbolTree.left , pnodeTabPage->rectSymbolTree.top , pnodeTabPage->rectSymbolTree.right-pnodeTabPage->rectSymbolTree.left , pnodeTabPage->rectSymbolTree.bottom-pnodeTabPage->rectSymbolTree.top , SWP_HIDEWINDOW );
				::ShowWindow( pnodeTabPage->hwndSymbolTree , SW_HIDE);
				::InvalidateRect( pnodeTabPage->hwndSymbolTree , NULL , TRUE );
				::UpdateWindow( pnodeTabPage->hwndSymbolTree );
			}

			if( pnodeTabPage->hwndQueryResultEdit )
			{
				::ShowWindow( pnodeTabPage->hwndQueryResultEdit , SW_HIDE );
				::UpdateWindow( pnodeTabPage->hwndQueryResultEdit );
			}

			if( pnodeTabPage->hwndQueryResultTable )
			{
				::ShowWindow( pnodeTabPage->hwndQueryResultTable , SW_HIDE );
				::UpdateWindow( pnodeTabPage->hwndQueryResultTable );
			}
		}
	}
	if( g_pnodeCurrentTabPage )
	{
		AdjustTabPageBox( g_pnodeCurrentTabPage );

		::SetWindowPos( g_pnodeCurrentTabPage->hwndScintilla , HWND_TOP , g_pnodeCurrentTabPage->rectScintilla.left , g_pnodeCurrentTabPage->rectScintilla.top , g_pnodeCurrentTabPage->rectScintilla.right-g_pnodeCurrentTabPage->rectScintilla.left , g_pnodeCurrentTabPage->rectScintilla.bottom-g_pnodeCurrentTabPage->rectScintilla.top , SWP_SHOWWINDOW );
		::InvalidateRect( g_pnodeCurrentTabPage->hwndScintilla , NULL , TRUE );
		::UpdateWindow( g_pnodeCurrentTabPage->hwndScintilla );

		if( g_pnodeCurrentTabPage->hwndSymbolList )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndSymbolList , HWND_TOP , g_pnodeCurrentTabPage->rectSymbolList.left , g_pnodeCurrentTabPage->rectSymbolList.top , g_pnodeCurrentTabPage->rectSymbolList.right-g_pnodeCurrentTabPage->rectSymbolList.left , g_pnodeCurrentTabPage->rectSymbolList.bottom-g_pnodeCurrentTabPage->rectSymbolList.top , SWP_SHOWWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndSymbolList , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndSymbolList , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndSymbolList );
		}

		if( g_pnodeCurrentTabPage->hwndSymbolTree )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndSymbolTree , HWND_TOP , g_pnodeCurrentTabPage->rectSymbolTree.left , g_pnodeCurrentTabPage->rectSymbolTree.top , g_pnodeCurrentTabPage->rectSymbolTree.right-g_pnodeCurrentTabPage->rectSymbolTree.left , g_pnodeCurrentTabPage->rectSymbolTree.bottom-g_pnodeCurrentTabPage->rectSymbolTree.top , SWP_SHOWWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndSymbolTree , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndSymbolTree , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndSymbolTree );
		}

		if( g_pnodeCurrentTabPage->hwndQueryResultEdit )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndQueryResultEdit , HWND_TOP , g_pnodeCurrentTabPage->rectQueryResultEdit.left , g_pnodeCurrentTabPage->rectQueryResultEdit.top , g_pnodeCurrentTabPage->rectQueryResultEdit.right-g_pnodeCurrentTabPage->rectQueryResultEdit.left , g_pnodeCurrentTabPage->rectQueryResultEdit.bottom-g_pnodeCurrentTabPage->rectQueryResultEdit.top , g_pnodeCurrentTabPage->hwndQueryResultEdit?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndQueryResultEdit , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndQueryResultEdit , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndQueryResultEdit );
		}

		if( g_pnodeCurrentTabPage->hwndQueryResultTable )
		{
			::SetWindowPos( g_pnodeCurrentTabPage->hwndQueryResultTable , HWND_TOP , g_pnodeCurrentTabPage->rectQueryResultListView.left , g_pnodeCurrentTabPage->rectQueryResultListView.top , g_pnodeCurrentTabPage->rectQueryResultListView.right-g_pnodeCurrentTabPage->rectQueryResultListView.left , g_pnodeCurrentTabPage->rectQueryResultListView.bottom-g_pnodeCurrentTabPage->rectQueryResultListView.top , g_pnodeCurrentTabPage->hwndQueryResultTable?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			::ShowWindow( g_pnodeCurrentTabPage->hwndQueryResultTable , SW_SHOW);
			::InvalidateRect( g_pnodeCurrentTabPage->hwndQueryResultTable , NULL , TRUE );
			::UpdateWindow( g_pnodeCurrentTabPage->hwndQueryResultTable );
		}

		::SetFocus( g_pnodeCurrentTabPage->hwndScintilla );
	}

	::InvalidateRect( g_hwndMainWindow , NULL , TRUE );
	::UpdateWindow( g_hwndMainWindow );

	return;
}

void OnResizeWindow( HWND hWnd , int nWidth , int nHeight )
{
	if( hwndFileTreeBar == NULL || g_hwndTabPages == NULL )
		return;

	g_rectTabPages.right = g_rectTabPages.left + nWidth ;
	g_rectTabPages.bottom = g_rectTabPages.top + nHeight ;

	UpdateAllWindows();

	return;
}

void PushOpenPathFilenameRecently( char *acPathFilename )
{
	int	index ;
	int	start ;
	int	end ;

	for( index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( strcmp( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] , acPathFilename ) == 0 )
			break;
	}
	if( index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT )
	{
		start = 0 ;
		end = index ;
	}
	else
	{
		start = 0 ;
		end = OPEN_PATHFILENAME_RECENTLY_MAXCOUNT - 1 ;
	}

	for( index = end - 1 ; index >= start ; index-- )
	{
		strcpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index+1] , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}
	strncpy( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[0] , acPathFilename , sizeof(g_stEditUltraMainConfig.aacOpenPathFilenameRecently[0])-1 );

	return;
}

void UpdateOpenPathFilenameRecently()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_FILE ;
	HMENU		hMenu_OPENRECENTLYFILES ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = ::GetMenu(g_hwndMainWindow) ;
	hMenu_FILE = ::GetSubMenu( hRootMenu , 0 ) ;
	hMenu_OPENRECENTLYFILES = ::GetSubMenu( hMenu_FILE , 2 ) ;
	count = ::GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = ::DeleteMenu( hMenu_OPENRECENTLYFILES , 0 , MF_BYPOSITION );
	}

	for( index = 0 ; index < OPEN_PATHFILENAME_RECENTLY_MAXCOUNT ; index++ )
	{
		if( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index][0] == '\0' )
			break;

		bret = ::AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_BASE+index , g_stEditUltraMainConfig.aacOpenPathFilenameRecently[index] );
	}

	return;
}

void SetViewTabWidthMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*p = NULL ;
	char		acMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	p = strchr( acMenuText , ' ' ) ;
	if( p == NULL )
	{
		snprintf( acMenuText+mii.cch , sizeof(acMenuText)-1-mii.cch , " : %d ..." , g_stEditUltraMainConfig.nTabWidth );
	}
	else
	{
		*(p) = '\0' ;
		mii.cch = (int)strlen(acMenuText) ;
		snprintf( acMenuText+mii.cch , sizeof(acMenuText)-1-mii.cch , " : %d ..." , g_stEditUltraMainConfig.nTabWidth );
	}
	mii.cch = (int)strlen(acMenuText) - 1 ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*p = NULL ;
	char		acMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	::GetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );
	p = strchr( acMenuText , '[' ) ;
	if( p )
	{
		*(p+1) = (g_stEditUltraMainConfig.nAutoCompletedShowAfterInputCharacters%10) + '0' ;
	}
	mii.cch = (int)strlen(acMenuText) - 1 ;
	::SetMenuItemInfo(::GetMenu(g_hwndMainWindow), nMenuId , FALSE, & mii );

	return;
}

void UpdateAllMenus( HWND hWnd , struct TabPage *pnodeTabPage )
{
	SetMenuItemChecked( hWnd , IDM_FILE_SETREADONLY_AFTER_OPEN , g_stEditUltraMainConfig.bSetReadOnlyAfterOpenFile );
	UpdateOpenPathFilenameRecently();
	SetMenuItemChecked( hWnd , IDM_FILE_UPDATE_WHERE_SELECTTABPAGE , g_stEditUltraMainConfig.bUpdateWhereSelectTabPage );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_WINDOWS_EOLS , (g_stEditUltraMainConfig.nNewFileEols==0) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_MAC_EOLS , (g_stEditUltraMainConfig.nNewFileEols==1) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_UNIX_EOLS , (g_stEditUltraMainConfig.nNewFileEols==2) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_UTF8 , (g_stEditUltraMainConfig.nNewFileEncoding==65001) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_GB18030 , (g_stEditUltraMainConfig.nNewFileEncoding==936) );
	SetMenuItemChecked( hWnd , IDM_FILE_NEWFILE_ENCODING_BIG5 , (g_stEditUltraMainConfig.nNewFileEncoding==950) );

	SetMenuItemChecked( hWnd , IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR , g_stEditUltraMainConfig.bEnableAutoAddCloseChar );
	SetMenuItemChecked( hWnd , IDM_EDIT_ENABLE_AUTO_INDENTATION , g_stEditUltraMainConfig.bEnableAutoIdentation );

	SetMenuItemChecked( hWnd , IDM_VIEW_FILETREE , bIsFileTreeBarShow );
	SetViewTabWidthMenuText( IDM_VIEW_TAB_WIDTH );
	SetMenuItemChecked( hWnd , IDM_VIEW_LINENUMBER_VISIABLE , g_stEditUltraMainConfig.bLineNumberVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_BOOKMARK_VISIABLE , g_stEditUltraMainConfig.bBookmarkVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_WHITESPACE_VISIABLE , g_stEditUltraMainConfig.bWhiteSpaceVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_NEWLINE_VISIABLE , g_stEditUltraMainConfig.bNewLineVisiable );
	SetMenuItemChecked( hWnd , IDM_VIEW_INDENTATIONGUIDES_VISIABLE , g_stEditUltraMainConfig.bIndentationGuidesVisiable );

	SetMenuItemChecked( hWnd , IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW , g_stEditUltraMainConfig.bEnableAutoCompletedShow );
	SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS );
	SetMenuItemChecked( hWnd , IDM_SOURCECODE_ENABLE_CALLTIPSHOW , g_stEditUltraMainConfig.bEnableCallTipShow );

	SetMenuItemChecked( hWnd , IDM_SOURCECODE_BLOCKFOLD_VISIABLE , g_stEditUltraMainConfig.bBlockFoldVisiable );
	
	if( pnodeTabPage )
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , pnodeTabPage && IsDocumentModified(pnodeTabPage) );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , pnodeTabPage && pnodeTabPage->acFilename[0] );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==0) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_MAC_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==1) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETEOLMODE,0,0)==2) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_UTF8 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==65001) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_GB18030 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==936) );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_ENCODING_BIG5 , (pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETCODEPAGE,0,0)==950) );

		SetMenuItemChecked( hWnd , IDM_VIEW_WRAPLINE_MODE , pnodeTabPage->pfuncScintilla(pnodeTabPage->pScintilla,SCI_GETWRAPMODE,0,0) );
	}
	else
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , FALSE );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , FALSE );

		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , FALSE );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_MAC_EOLS , FALSE );
		SetMenuItemChecked( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , FALSE );

		SetMenuItemChecked( hWnd , IDM_VIEW_WRAPLINE_MODE , FALSE );
	}

	return;
}

int BeforeWndProc( MSG *p_msg )
{
	int		nret = 0 ;

	if( p_msg->message == WM_MOUSEMOVE )
	{
		WPARAM wParam ;
		LPARAM lParam ;
		POINT pt ;
		wParam = p_msg->wParam ;
		lParam = p_msg->lParam ;
		pt.x = LOWORD(lParam) ;
		pt.y = HIWORD(lParam) ;
		if( p_msg->hwnd != g_hwndMainWindow )
		{
			::ClientToScreen( p_msg->hwnd , & pt );
			::ScreenToClient( g_hwndMainWindow , & pt );
			lParam = MAKELONG( pt.x , pt.y ) ;
		}
		wParam = MAKELONG( IDM_MOUSEMOVE , 0 ) ;
		::PostMessage( g_hwndMainWindow , WM_COMMAND , wParam , lParam);
	}

	if( p_msg->hwnd == hwndFileTree )
	{
		if( p_msg->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( hFileTreePopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_REFRESH_FILETREE )
		{
			RefreshFileTree();
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_hwndTabPages )
	{
		if( p_msg->message == WM_LBUTTONDOWN )
		{
			if( g_bIsTabPageMoving == FALSE )
			{
				int x = LOWORD(p_msg->lParam) ;
				int y = HIWORD(p_msg->lParam) ;

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage ;
				g_nTabPageMoveFrom = -1 ;
				for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
				{
					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						g_nTabPageMoveFrom = nTabPageIndex ;
						break;
					}
				}
				if( g_nTabPageMoveFrom >= 0 )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					g_bIsTabPageMoving = TRUE ;
					g_rectLMouseDown.x = x ;
					g_rectLMouseDown.y = y ;
				}
			}
		}
		else if( p_msg->message == WM_LBUTTONUP )
		{
			if( g_bIsTabPageMoving == TRUE )
			{
				int x = LOWORD(p_msg->lParam) ;
				int y = HIWORD(p_msg->lParam) ;

				if( abs(x-g_rectLMouseDown.x) > MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK || abs(y-g_rectLMouseDown.y) > MOUSE_MOVE_THRESHOLD_FROM_DRAG_OR_CLICK )
				{
					int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
					RECT rectTabPage ;
					int nTabPageMoveTo = -1 ;
					for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
					{
						TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
						if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
						{
							nTabPageMoveTo = nTabPageIndex ;
							break;
						}
					}
					if( nTabPageMoveTo >= 0 )
					{
						TabCtrl_DeleteItem( g_hwndTabPages , g_nTabPageMoveFrom );

						TCITEM		tci ;

						memset( & tci , 0x00 , sizeof(TCITEM) );
						tci.mask = TCIF_TEXT|TCIF_PARAM ;
						tci.pszText = g_pnodeCurrentTabPage->acFilename ;
						tci.lParam = (LPARAM)g_pnodeCurrentTabPage ;
						nret = TabCtrl_InsertItem( g_hwndTabPages , nTabPageMoveTo , & tci ) ;
						if( nret == -1 )
						{
							::MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
						}
						else
						{
							SelectTabPageByIndex( nTabPageMoveTo );
						}
					}
				}

				::SetCursor( LoadCursor(NULL,IDC_ARROW) );
				g_bIsTabPageMoving = FALSE ;
			}
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		if( p_msg->message == WM_KEYDOWN )
		{
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , p_msg->wParam );
		}
		else if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hEditorPopupMenu , 0 , pt.x , pt.y , 0 , g_hwndMainWindow , NULL );
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolList )
	{
		if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hFunctionListPopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RELOAD_FUNCTIONLIST )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolList( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolTree )
	{
		if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hTreeViewPopupMenu , 0 , pt.x , pt.y , 0 , p_msg->hwnd , NULL );
			return 1;
		}
		else if( p_msg->message == WM_COMMAND && LOWORD(p_msg->wParam) == IDM_RELOAD_TREEVIEW )
		{
			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnReloadSymbolTree( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndSymbolTree )
	{
		NMHDR		*lpnmhdr = (NMHDR*)(p_msg->lParam) ;

		if( p_msg->message == WM_LBUTTONDBLCLK )
		{
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree( g_pnodeCurrentTabPage );
			return 1;
		}
	}

	return 0;
}

int AfterWndProc( MSG *p_msg )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	struct TabPage	*p = NULL ;
	TCITEM		tci ;

	if( g_pnodeCurrentTabPage && p_msg->hwnd == g_hwndTabPages )
	{
		if( p_msg->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_msg->lParam) ;
			pt.y = HIWORD(p_msg->lParam) ;
			::ClientToScreen( p_msg->hwnd , & pt );
			::TrackPopupMenu( g_hTabPagePopupMenu , 0 , pt.x , pt.y , 0 , g_hwndMainWindow , NULL );
		}
	}

	if( p_msg->message == WM_LBUTTONUP )
	{
		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			p = (struct TabPage *)(tci.lParam);
			if( p_msg->hwnd == p->hwndScintilla )
			{
				int nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
				AddNavigateBackNextTrace( p , nCurrentPos );

				if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp )
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp( g_pnodeCurrentTabPage , p_msg->wParam );
			}
		}
	}
	else if( p_msg->message == WM_KEYUP && g_pnodeCurrentTabPage && p_msg->hwnd == g_pnodeCurrentTabPage->hwndScintilla )
	{
		int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		UpdateNavigateBackNextTrace( g_pnodeCurrentTabPage , nCurrentPos );

		if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp )
			g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyUp( g_pnodeCurrentTabPage , p_msg->wParam );
	}
	
	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		nWidth , nHeight ;
	int		wmId , wmNC ;
	NMHDR		*lpnmhdr = NULL;
	SCNotification	*lpnotify = NULL ;
	NMTREEVIEW	*lpnmtv = NULL ;
	PAINTSTRUCT	ps ;
	HDC		hdc ;

	int		nret = 0;

	if( message == WM_COMMAND )
	{
		wmId = LOWORD(wParam) ;
		if( IDM_FILE_OPEN_RECENTLY_BASE <= wmId && wmId <= IDM_FILE_OPEN_RECENTLY_BASE + OPEN_PATHFILENAME_RECENTLY_MAXCOUNT-1 )
		{
			OpenFileDirectly( g_stEditUltraMainConfig.aacOpenPathFilenameRecently[wmId-IDM_FILE_OPEN_RECENTLY_BASE] );
		}
	}

	switch (message)
	{
	case WM_CREATE:
		nret = OnCreateWindow( hWnd , wParam , lParam ) ;
		if( nret )
			PostQuitMessage(0);
		break;
	case WM_SIZE:
		nWidth = LOWORD( lParam );
		nHeight = HIWORD( lParam );
		OnResizeWindow( hWnd , nWidth , nHeight );
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam) ;
		wmNC = HIWORD(wParam) ;
		// 分析菜单选择:
		if( wmNC == LBN_DBLCLK && g_pnodeCurrentTabPage )
		{
			return OnFunctionListDbClick( g_pnodeCurrentTabPage );
		}
		switch (wmId)
		{
		case IDM_FILE_NEW:
			OnNewFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_OPEN:
			OnOpenFile();
			break;
		case IDM_FILE_SAVE:
			OnSaveFile( g_pnodeCurrentTabPage , FALSE );
			break;
		case IDM_FILE_SAVEAS:
			OnSaveFileAs( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_SAVEALL:
			OnSaveAllFiles();
			break;
		case IDM_FILE_CLOSE:
			OnCloseFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_CLOSEALL:
			OnCloseAllFile();
			break;
		case IDM_FILE_CLOSEALL_EXPECT_CURRENT_FILE:
			OnCloseAllExpectCurrentFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_REMOTE_FILESERVERS:
			OnManageRemoteFileServers();
			break;
		case IDM_FILE_SETREADONLY_AFTER_OPEN:
			OnSetReadOnlyAfterOpenFile( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_UPDATE_WHERE_SELECTTABPAGE:
			OnFileUpdateWhereSelectTabPage( g_pnodeCurrentTabPage );
			break;
		case IDM_FILE_NEWFILE_WINDOWS_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 0 );
			break;
		case IDM_FILE_NEWFILE_MAC_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 1 );
			break;
		case IDM_FILE_NEWFILE_UNIX_EOLS:
			OnFileSetNewFileEols( g_pnodeCurrentTabPage , 2 );
			break;
		case IDM_FILE_CONVERT_WINDOWS_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 0 );
			break;
		case IDM_FILE_CONVERT_MAC_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 1 );
			break;
		case IDM_FILE_CONVERT_UNIX_EOLS:
			OnFileConvertEols( g_pnodeCurrentTabPage , 2 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_UTF8:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_UTF8 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_GB18030:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_GBK );
			break;
		case IDM_FILE_NEWFILE_ENCODING_BIG5:
			OnFileSetNewFileEncoding( g_pnodeCurrentTabPage , ENCODING_BIG5 );
			break;
		case IDM_FILE_CONVERT_ENCODING_UTF8:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_UTF8 );
			break;
		case IDM_FILE_CONVERT_ENCODING_GB18030:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_GBK );
			break;
		case IDM_FILE_CONVERT_ENCODING_BIG5:
			OnFileConvertEncoding( g_pnodeCurrentTabPage , ENCODING_BIG5 );
			break;
		case IDM_EXIT:
			nret = CheckAllFilesSave() ;
			if( nret )
				break;
			DestroyWindow(hWnd);
			break;
		case IDM_EDIT_UNDO:
			OnUndoEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_REDO:
			OnRedoEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUT:
			OnCutEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPY:
			OnCopyEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTE:
			OnPasteEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETE:
			OnDeleteEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUTLINE:
			OnCutLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_CUTLINE_AND_PASTELINE:
			OnCutLineAndPasteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPYLINE:
			OnCopyLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_COPYLINE_AND_PASTELINE:
			OnCopyLineAndPasteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTELINE:
			OnPasteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_PASTELINE_UPSTAIRS:
			OnPasteLineUpstairsEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_DELETELINE:
			OnDeleteLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_JOINLINE:
			OnJoinLineEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_LOWERCASE:
			OnLowerCaseEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_UPPERCASE:
			OnUpperCaseEdit( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR:
			OnEditEnableAutoAddCloseChar( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_ENABLE_AUTO_INDENTATION:
			OnEditEnableAutoIdentation( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_BASE64_ENCODING:
			OnEditBase64Encoding( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_BASE64_DECODING:
			OnEditBase64Decoding( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_MD5:
			OnEditMd5( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_SHA1:
			OnEditSha1( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_SHA256:
			OnEditSha256( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_3DES_CBC_ENCRYPTO:
			OnEdit3DesCbcEncrypto( g_pnodeCurrentTabPage );
			break;
		case IDM_EDIT_3DES_CBC_DECRYPTO:
			OnEdit3DesCbcDecrypto( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FIND:
			OnSearchFind( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FINDPREV:
			OnSearchFindPrev( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_FINDNEXT:
			OnSearchFindNext( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REPLACE:
			OnSearchReplace( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTALL:
			OnSelectAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTWORD:
			OnSelectWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_SELECTLINE:
			OnSelectLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_CHARGROUP:
			OnAddSelectLeftCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_CHARGROUP:
			OnAddSelectRightCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_WORD:
			OnAddSelectLeftWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_WORD:
			OnAddSelectRightWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_TOP_BLOCKFIRSTLINE:
			OnAddSelectTopBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADDSELECT_BOTTOM_BLOCKFIRSTLINE:
			OnAddSelectBottomBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_LEFT_CHARGROUP:
			OnMoveLeftCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_RIGHT_CHARGROUP:
			OnMoveRightCharGroup( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_LEFT_WORD:
			OnMoveLeftWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_RIGHT_WORD:
			OnMoveRightWord( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_TOP_BLOCKFIRSTLINE:
			OnMoveTopBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_MOVE_BOTTOM_BLOCKFIRSTLINE:
			OnMoveBottomBlockFirstLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_TOGGLE_BOOKMARK:
			OnSearchToggleBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_ADD_BOOKMARK:
			OnSearchAddBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REMOVE_BOOKMARK:
			OnSearchRemoveBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_REMOVE_ALL_BOOKMARKS:
			OnSearchRemoveAllBookmarks( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK:
			OnSearchGotoPrevBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK:
			OnSearchGotoNextBookmark( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoPrevBookmarkInAllFiles( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoNextBookmarkInAllFiles( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_GOTOLINE:
			OnSearchGotoLine( g_pnodeCurrentTabPage );
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE:
			OnSearchNavigateBackPrev_InThisFile();
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_ALL_FILES:
			OnSearchNavigateBackPrev_InAllFiles();
			break;
		case IDM_VIEW_FILETREE:
			OnViewFileTree( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_STYLETHEME:
			OnViewStyleTheme( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_WRAPLINE_MODE:
			OnViewWrapLineMode( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_TAB_WIDTH:
			OnViewTabWidth( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_LINENUMBER_VISIABLE:
			OnViewLineNumberVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_BOOKMARK_VISIABLE:
			OnViewBookmarkVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_WHITESPACE_VISIABLE:
			OnViewWhiteSpaceVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_NEWLINE_VISIABLE:
			OnViewNewLineVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_INDENTATIONGUIDES_VISIABLE:
			OnViewIndentationGuidesVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMOUT:
			OnViewZoomOut( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMIN:
			OnViewZoomIn( g_pnodeCurrentTabPage );
			break;
		case IDM_VIEW_ZOOMRESET:
			OnViewZoomReset( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_VISIABLE:
			OnSourceCodeBlockFoldVisiable( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_TOGGLE:
			OnSourceCodeBlockFoldToggle( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACT:
			OnSourceCodeBlockFoldContract( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPAND:
			OnSourceCodeBlockFoldExpand( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACTALL:
			OnSourceCodeBlockFoldContractAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPANDALL:
			OnSourceCodeBlockFoldExpandAll( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_GOTODEF:
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F11 );
			break;
		case IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW:
			OnSourceCodeEnableAutoCompletedShow( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS:
			OnSourceCodeAutoCompletedShowAfterInputCharacters( g_pnodeCurrentTabPage );
			break;
		case IDM_SOURCECODE_ENABLE_CALLTIPSHOW:
			OnSourceCodeEnableCallTipShow( g_pnodeCurrentTabPage );
			break;
		case IDM_DATABASE_INSERT_DATABASE_CONNECTION_CONFIG:
			OnInsertDataBaseConnectionConfig( g_pnodeCurrentTabPage );
			break;
		case IDM_DATABASE_EXECUTE_SQL:
			if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown )
				g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnKeyDown( g_pnodeCurrentTabPage , VK_F5 );
			break;
		case IDM_DATABASE_INSERT_REDIS_CONNECTION_CONFIG:
			OnInsertRedisConnectionConfig( g_pnodeCurrentTabPage );
			break;
		case IDM_ENV_FILE_POPUPMENU:
			OnEnvFilePopupMenu();
			break;
		case IDM_ENV_DIRECTORY_POPUPMENU:
			OnEnvDirectoryPopupMenu();
			break;
		case IDM_ENV_SET_PROCESSFILE_CMD:
			OnEnvSetProcessFileCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSFILE_CMD:
			OnEnvExecuteProcessFileCommand( g_pnodeCurrentTabPage );
			break;
		case IDM_ENV_SET_PROCESSTEXT_CMD:
			OnEnvSetProcessTextCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSTEXT_CMD:
			OnEnvExecuteProcessTextCommand( g_pnodeCurrentTabPage );
			break;
		case IDM_ABOUT:
			DialogBox(g_hAppInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_MOUSEMOVE:
			if( bIsFileTreeBarShow == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsFileTreeBarResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( rectFileTreeBar.top <= y && y <= rectFileTreeBar.bottom && rectFileTreeBar.right < x && x < g_rectTabPages.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					if( g_bIsFileTreeBarHoverResizing == FALSE )
						g_bIsFileTreeBarHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsFileTreeBarHoverResizing == TRUE )
						g_bIsFileTreeBarHoverResizing = FALSE ;
				}

				if( g_bIsFileTreeBarResizing == TRUE )
				{
					g_stEditUltraMainConfig.nFileTreeBarWidth = x - rectFileTreeBar.left - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nFileTreeBarWidth < FILETREEBAR_WIDTH_MIN )
						g_stEditUltraMainConfig.nFileTreeBarWidth = FILETREEBAR_WIDTH_MIN ;

					UpdateAllWindows();
				}
			}

			if( g_pnodeCurrentTabPage && g_bIsTabPageMoving == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				int nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				RECT rectTabPage ;
				for( int nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
				{
					TabCtrl_GetItemRect( g_hwndTabPages , nTabPageIndex , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						::SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					}
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndSymbolList )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsFunctionListResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_pnodeCurrentTabPage->rectSymbolList.top <= y && y <= g_pnodeCurrentTabPage->rectSymbolList.bottom && g_pnodeCurrentTabPage->rectScintilla.right < x && x < g_pnodeCurrentTabPage->rectSymbolList.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					g_bIsFunctionListHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsFunctionListHoverResizing == TRUE )
						g_bIsFunctionListHoverResizing = FALSE ;
				}

				if( g_bIsFunctionListResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSymbolListWidth = g_pnodeCurrentTabPage->rectSymbolList.right - x - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSymbolListWidth < SYMBOLLIST_WIDTH_MIN )
						g_stEditUltraMainConfig.nSymbolListWidth = SYMBOLLIST_WIDTH_MIN ;

					UpdateAllWindows();
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndSymbolTree )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsTreeViewResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( g_pnodeCurrentTabPage->rectSymbolTree.top <= y && y <= g_pnodeCurrentTabPage->rectSymbolTree.bottom && g_pnodeCurrentTabPage->rectScintilla.right < x && x < g_pnodeCurrentTabPage->rectSymbolTree.left )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					g_bIsTreeViewHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsTreeViewHoverResizing == TRUE )
						g_bIsTreeViewHoverResizing = FALSE ;
				}

				if( g_bIsTreeViewResizing == TRUE )
				{
					g_stEditUltraMainConfig.nSymbolTreeWidth = g_pnodeCurrentTabPage->rectSymbolTree.right - x - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSymbolTreeWidth < TREEVIEW_WIDTH_MIN )
						g_stEditUltraMainConfig.nSymbolTreeWidth = TREEVIEW_WIDTH_MIN ;

					UpdateAllWindows();
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndQueryResultEdit )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsSqlQueryResultEditResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( g_pnodeCurrentTabPage->rectQueryResultEdit.left <= x && x <= g_pnodeCurrentTabPage->rectQueryResultEdit.right && g_pnodeCurrentTabPage->rectScintilla.bottom < y && y < g_pnodeCurrentTabPage->rectQueryResultEdit.top )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					g_bIsSqlQueryResultEditHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
						g_bIsSqlQueryResultEditHoverResizing = FALSE ;
				}

				if( g_bIsSqlQueryResultEditResizing == TRUE )
				{
					/*
					g_stEditUltraMainConfig.nSqlQueryResultListViewHeight = g_rectTabPages.bottom - y - SPLIT_WIDTH - g_stEditUltraMainConfig.nSqlQueryResultEditHeight - SPLIT_WIDTH ;
					if( g_stEditUltraMainConfig.nSqlQueryResultListViewHeight < SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
						g_stEditUltraMainConfig.nSqlQueryResultListViewHeight = SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN ;
					*/
					g_stEditUltraMainConfig.nSqlQueryResultEditHeight = g_pnodeCurrentTabPage->rectQueryResultEdit.bottom - y - SPLIT_WIDTH/2 ;
					if( g_stEditUltraMainConfig.nSqlQueryResultEditHeight < SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
						g_stEditUltraMainConfig.nSqlQueryResultEditHeight = SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN ;

					UpdateAllWindows();
				}
			}

			if( g_pnodeCurrentTabPage && g_pnodeCurrentTabPage->hwndQueryResultTable )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( g_bIsSqlQueryResultListViewResizing == TRUE )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( g_pnodeCurrentTabPage->rectQueryResultListView.left <= x && x <= g_pnodeCurrentTabPage->rectQueryResultListView.right && g_pnodeCurrentTabPage->rectQueryResultEdit.bottom < y && y < g_pnodeCurrentTabPage->rectQueryResultListView.top )
				{
					::SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					g_bIsSqlQueryResultListViewHoverResizing = TRUE ;
				}
				else
				{
					if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
						g_bIsSqlQueryResultListViewHoverResizing = FALSE ;
				}

				if( g_bIsSqlQueryResultListViewResizing == TRUE )
				{
					int	nListViewHeight = g_pnodeCurrentTabPage->rectQueryResultListView.bottom - y - SPLIT_WIDTH/2 ;
					// int	nEditHeight = g_pnodeCurrentTabPage->rectQueryResultListView.bottom-nListViewHeight - g_pnodeCurrentTabPage->rectQueryResultEdit.top - SPLIT_WIDTH/2 ;
					// if( nEditHeight >= SQLQUERYRESULT_EDIT_HEIGHT_MIN && nListViewHeight >= SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
					if( nListViewHeight >= SQLQUERYRESULT_LISTVIEW_HEIGHT_MIN )
					{
						// g_stEditUltraMainConfig.nSqlQueryResultEditHeight = nEditHeight ;
						g_stEditUltraMainConfig.nSqlQueryResultListViewHeight = nListViewHeight ;
					}

					UpdateAllWindows();
				}
			}

			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		break;
	case WM_NOTIFY:
		lpnmhdr = (NMHDR*)lParam ;
		lpnotify = (SCNotification*)lParam ;
		lpnmtv = (NMTREEVIEW*)lParam ;

		switch (lpnmhdr->code)
		{
		case SCN_CHARADDED:
			OnCharAdded( g_pnodeCurrentTabPage , lpnotify );
			break;
		case TCN_SELCHANGE:
			OnSelectChange();
			break;
		case SCN_SAVEPOINTREACHED:
			OnSavePointReached( g_pnodeCurrentTabPage );
			break;
		case SCN_SAVEPOINTLEFT:
			OnSavePointLeft( g_pnodeCurrentTabPage );
			break;
		case SCN_MARGINCLICK:
			OnMarginClick( g_pnodeCurrentTabPage , lpnotify );
			break;
		case TVN_ITEMEXPANDING:
			if( lpnmhdr->hwndFrom == hwndFileTree )
			{
				OnFileTreeNodeExpanding( lpnmtv );
			}
			break;
		case NM_DBLCLK:
			if( lpnmhdr->hwndFrom == hwndFileTree )
			{
				nret = OnFileTreeNodeDbClick( lpnmtv ) ;
				if( nret == 1 )
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
			else if( g_pnodeCurrentTabPage && lpnmhdr->hwndFrom == g_pnodeCurrentTabPage->hwndSymbolTree )
			{
				if( g_pnodeCurrentTabPage->pstDocTypeConfig && g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree )
					g_pnodeCurrentTabPage->pstDocTypeConfig->pfuncOnDbClickSymbolTree( g_pnodeCurrentTabPage );
				return 0;
			}
			break;
		}

		break;
	case WM_COPYDATA:
		{
			COPYDATASTRUCT	*cpd ;
			cpd = (COPYDATASTRUCT*)lParam ;
			char *acPathFilename = (char*)(cpd->lpData) ;
			OpenFileOrDirectoryDirectly( acPathFilename );
		}

		{
			HWND hForeWnd = NULL; 
			DWORD dwForeID; 
			DWORD dwCurID; 

			hForeWnd = GetForegroundWindow(); 
			dwCurID = GetCurrentThreadId(); 
			dwForeID = GetWindowThreadProcessId( hForeWnd, NULL ); 
			AttachThreadInput( dwCurID, dwForeID, TRUE); 
			ShowWindow( g_hwndMainWindow, SW_SHOWNORMAL ); 
			/*
			SetWindowPos( hwndMainClient, HWND_TOPMOST, 0,0,0,0, SWP_NOSIZE|SWP_NOMOVE ); 
			SetWindowPos( hwndMainClient, HWND_NOTOPMOST, 0,0,0,0, SWP_NOSIZE|SWP_NOMOVE ); 
			*/
			SetForegroundWindow( g_hwndMainWindow ); 
			AttachThreadInput( dwCurID, dwForeID, FALSE);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_ACTIVATE:
		if( g_pnodeCurrentTabPage )
		{
			if( hWnd == g_hwndMainWindow )
			{
				OnSelectChange();
			}
		}
		break;
	case WM_LBUTTONDOWN:
		if( g_bIsFileTreeBarHoverResizing == TRUE )
			g_bIsFileTreeBarResizing = TRUE ;

		if( g_bIsFunctionListHoverResizing == TRUE )
			g_bIsFunctionListResizing = TRUE ;

		if( g_bIsTreeViewHoverResizing == TRUE )
			g_bIsTreeViewResizing = TRUE ;

		if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
			g_bIsSqlQueryResultEditResizing = TRUE ;

		if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
			g_bIsSqlQueryResultListViewResizing = TRUE ;

		break;
	case WM_LBUTTONUP:
		if( g_bIsFileTreeBarResizing == TRUE )
		{
			g_bIsFileTreeBarResizing = FALSE ;
			UpdateAllWindows();
		}

		if( g_bIsFunctionListResizing == TRUE )
		{
			g_bIsFunctionListResizing = FALSE ;
			UpdateAllWindows();
		}

		if( g_bIsTreeViewResizing == TRUE )
		{
			g_bIsTreeViewResizing = FALSE ;
			UpdateAllWindows();
		}

		if( g_bIsSqlQueryResultEditHoverResizing == TRUE )
		{
			g_bIsSqlQueryResultEditResizing = FALSE ;
			UpdateAllWindows();
		}

		if( g_bIsSqlQueryResultListViewHoverResizing == TRUE )
		{
			g_bIsSqlQueryResultListViewResizing = FALSE ;
			UpdateAllWindows();
		}

		break;
	case WM_DROPFILES:
		OnOpenDropFile( (HDROP)wParam );
		break;
	case WM_CLOSE:
		if( hWnd == g_hwndMainWindow )
		{
			nret = CheckAllFilesSave() ;
			if( nret )
				break;
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		CenterWindow( hDlg , g_hwndMainWindow );
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
